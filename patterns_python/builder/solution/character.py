
class Character:
    def __init__(self):
        self.profession = None
        self.race = "human"
        self.stg = None
        self.dex = None
        self.con = None
        self.int = None
        self.wis = None
        self.cha = None

        self.weapon = None
        self.armour = None

    def __repr__(self):

        return f"""
        o--{{======> Your character  <======}}--o
            Class: {self.profession} Race: {self.race}
            💪 Strength: {self.stg}
            🤸 Dexterity: {self.dex}
            🏋️ Constitution: {self.con}
            🧠 Intelligence: {self.int}
            🦉 Wisdom: {self.wis}
            🎭 Charisma: {self.cha}
            -------------------------
            ⚔️ Weapon: {self.weapon}
            🛡️ Armour: {self.armour}
        """
