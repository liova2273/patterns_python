from random import randint
from typing import List

import matplotlib.pyplot as plt


class CryptoPrice:

    def __init__(self, symbol: str):
        self.symbol = symbol
        self._values: List = []

    @property
    def values(self):
        return self._values

    def sample(self):
        self._values.append(
            randint(23_000, 30_000)
        )


def myapp():
    bitcoin_price = CryptoPrice("bitcoin")
    ethereum_price = CryptoPrice("ethereum")
    tether_price = CryptoPrice("tether")

    for _ in range(10):
        bitcoin_price.sample()
        ethereum_price.sample()
        tether_price.sample()

    fig, axes = plt.subplots(2, 1)
    fig.suptitle('Crypto currency comparison', fontsize=16)
    axes[0].plot(bitcoin_price.values)
    axes[0].grid()
    axes[0].set_title("bitcoin")
    axes[1].plot(ethereum_price.values)
    axes[1].grid()
    axes[1].set_title("ethereum")
    plt.draw()
    plt.pause(0.001)

    fig, axes = plt.subplots(2, 1)
    fig.suptitle('Crypto currency comparison', fontsize=16)
    axes[0].plot(bitcoin_price.values)
    axes[0].grid()
    axes[0].set_title("bitcoin")
    axes[1].plot(tether_price.values)
    axes[1].grid()
    axes[1].set_title("tether")
    plt.show()


if __name__ == "__main__":
    myapp()
