from abc import ABC, abstractmethod


class Observer(ABC):

    @abstractmethod
    def update(self, origin, value):
        raise NotImplementedError("Not yet")
