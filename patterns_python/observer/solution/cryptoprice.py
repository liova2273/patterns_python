from random import randint
from typing import Dict, List

from patterns_python.observer.solution.observer import (
    Observer
)


class ObservableCryptoPrice:

    def __init__(self, symbol: str):
        self._observers: Dict = {}
        self.symbol = symbol
        self._values: List = []

    def attach(self, observer: Observer):
        self._observers[id(observer)] = observer

    def detach(self, observer: Observer):
        del(self._observers[id(observer)])

    def notify(self):
        if self._observers:
            for observer in self._observers.values():
                observer.update(self.symbol, self._values)


class CryptoPrice(ObservableCryptoPrice):

    @property
    def values(self):
        return self._values

    # In a real world application, likely you would like here
    # a call to an external API for getting the real value
    # of the different crypto currencies
    def sample(self):
        self._values.append(
            randint(23_000, 30_000)
        )

        self.notify()
