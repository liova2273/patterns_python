import logging

logging.basicConfig(level="INFO")

internal_logger = logging.getLogger("internal")
external_logger = logging.getLogger("external")
app_logger = logging.getLogger("myapp")
