from patterns_python.abstract_factory.problem.urgent_notification import (
    UrgentNotification
)
from patterns_python.abstract_factory.problem.daily_notification import (
    DailyNotification
)
from patterns_python.abstract_factory.loggers import app_logger


class Client:

    def __init__(self, client_id: str):
        self.id = client_id
        self.urgent_notifications = UrgentNotification()
        self.daily_notifications = DailyNotification()

    def offer_client(self):
        self.urgent_notifications.notify()

    def queue_order(self):
        self.daily_notifications.notify()

    def process_daily_notifications(self):
        self.daily_notifications.dispatch()

    def process(self, op: str):
        if op == "1":
            self.offer_client()
        elif op == "2":
            self.queue_order()
        elif op == "3":
            self.process_daily_notifications()
        else:
            app_logger.info("Nothing to do")


def myapp():
    client_id = input("Insert client id: ")
    my_client = Client(client_id=client_id)

    while True:
        op = input(
            """Select operation number:
                [1] get an offer
                [2] queue an offer
                [3] dispatch offers of the day
                [x] exit
            \r>> """
        )

        if op == "x":
            exit(0)
        else:
            my_client.process(op)


if __name__ == "__main__":
    myapp()
