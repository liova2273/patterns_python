from datetime import datetime
from patterns_python.abstract_factory.loggers import app_logger


# Concrete Daily Notification
class DailyNotification:

    def __init__(self):
        self.store = {}

    def notify(self):
        self.store[datetime.now()] = "There is an internal notification"

    def dispatch(self):
        for date, message in self.store.items():
            if date < datetime.today():
                app_logger.info(message)
