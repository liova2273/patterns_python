from abc import ABC, abstractmethod
from patterns_python.abstract_factory.loggers import (
    internal_logger,
    external_logger
)


# Abstract class
class UrgentNotification(ABC):

    @abstractmethod
    def notify(self):
        raise NotImplementedError("not yet")


# Concrete Internal Urgent Notification
class InternalUrgentNotification(UrgentNotification):
    def notify(self):
        internal_logger.warning("There is an urgent notification")


# Concrete External Urgent Notification
class ExternalUrgentNotification(UrgentNotification):
    def notify(self):
        external_logger.warning("There is an urgent notification")
