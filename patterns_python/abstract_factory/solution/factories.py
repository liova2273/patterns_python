from abc import ABC, abstractmethod
from patterns_python.abstract_factory.solution.urgent_notification import (
    InternalUrgentNotification,
    ExternalUrgentNotification
)
from patterns_python.abstract_factory.solution.daily_notification import (
    InternalDailyNotification,
    ExternalDailyNotification
)


# Abstract factory
class NotificationFactory(ABC):

    @abstractmethod
    def create_urgent_notification(self):
        raise NotImplementedError("Not yet")

    @abstractmethod
    def create_daily_notification(self):
        raise NotImplementedError("Not yet")


# Concrete implementation
class InternalNotificationFactory(NotificationFactory):

    def create_urgent_notification(self):
        return InternalUrgentNotification()

    def create_daily_notification(self):
        return InternalDailyNotification()


# Concrete implementation
class ExternalNotificationFactory(NotificationFactory):

    def create_urgent_notification(self):
        return ExternalUrgentNotification()

    def create_daily_notification(self):
        return ExternalDailyNotification()
