from typing import List


class BasicPrompt:

    def __init__(self):
        self.format = ">>"

    def print(self):
        return self.format


class Shell:

    def __init__(self, shell_prompt: BasicPrompt):
        self.prompt = shell_prompt
        self.history: List = []
        self.last_command = None

    def input(self):
        self.last_command = input(self.prompt.print())
        self.history.append(self.last_command)

        self.execute()

    def execute(self):
        if self.last_command == "history":
            for index, command in enumerate(self.history):
                print(f"[{index}] {command}")
        if self.last_command == "exit":
            exit(0)


def myapp():
    my_shell = Shell(BasicPrompt())
    while True:
        my_shell.input()


if __name__ == "__main__":
    myapp()
