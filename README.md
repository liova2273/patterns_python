# About me...
Javier Martinez Alcantara, Javi.

[LinkedIn](https://www.linkedin.com/in/javier-martinez-alcantara/)

[Some info](https://jmaralc.github.io/)

# 🤠 Design patterns in python: only the footsteps of God will proceed...

This is the **second** repo of a series of repos related to three key topics in the life of a developer. 
I took the chance to honor the hero of my childhood, Indiana Jones, naming them after the three trials he passed 
in the movie *the Last Crusade*...is not a random choice but rather the idea that developing is in 
itself full of dangers, adventures and the promise of excitement.

Let me introduce you these key points:
- Testing [15 April 2021]
  [Video](https://www.youtube.com/watch?v=6mcil5cbsV4) 
  //
  [Repo](https://gitlab.com/jmaralc/testing_python)
    
- **Design patterns**(this repo) [14 June 2021]
- Architecture [15 September 2021]

These three are trials that we have in our daily life and there are tons of bibliography and documentation out there. 
What I pretend here is to treat these topics in an elegant, applied and enjoyable way...and as rigorous as possible.

My wish is that you enjoy and take some learnings home. The adventure could start just by cloning this repo.

⚠️ **IMPORTANT**⚠️
Review all the branches and try to follow the slides to understand the flow of the session.

# Motivation

When we talk about design patterns we refer to tested and proved solutions for common situations and problems that we can encounter when designing our code.
In my humble opinion is where the engineering part of our job come to play. In a world of framework we are guided to certain practices but as professionals
we need to know the foundations of these practices to extend them beyond the framework also to our own designs. 

Two benefits pop up from the use and study of design patterns:
- A common language that can be understood worldwide
- A clean and scalable base code that will improve the quality of our work
 

The session for today is focused in a subset of the design patterns presented in the *gang of four* book. We will review the problems that pop up with a poor design,
and we will review together an implementation of the pattern. Enjoy!

# [Session slides](https://docs.google.com/presentation/d/1L9yK3S4A17xFySd8i6_fsPJUaaVFOyAeM-95mfG-8vo/edit?usp=sharing)

# Setup

For this talk you will need python 3.9 and pip installed in your computer. We will use pipenv for installing the packages and keeping the virtual environment. So I recommend you to install it with pip:

```shell
pip install --user --upgrade pipenv
```
Further information about installation [here](https://pipenv-fork.readthedocs.io/en/latest/install.html#installing-pipenv)

# Global Goal

- Spread the knowledge of python
- Promote pythonistas awareness and adoption of good practices and technologies
- Facilitate safe spaces for people to show and talk 

# Structure of the repository/project

This repo present just one branch, and the different examples of patterns have been distributed in folders within `patterns_python` forlder.
So we have:

- **abstract_fatory** → provide an interface for creating families of related or dependent objects without specifying their concrete classes

- **builder** -> separate the construction of a complex object from its representation so that the same construction process can create different representations.

- **decorator** -> attach additional responsibilities to an object dynamically.

- **observer** -> define a one-to-many dependency between objets so that when one object changes state, all its dependents are notified and updated automatically.

Each of these folders contains a `problem` folder, that will illustrate the problem that the pattern solves, 
and a `solution` folder that contains the code that implements the pattern.

# How to run it
First set in place pipenv with:

```shell
pipenv install
```

Within the `Pipfile` you will find a section called scripts. In that section you will find a shortcut for calling the needed command to perform an action.
For instance to execute the code related to the problem that observer solved, run:
```shell
pipenv run observer_problem
```

In the other hand to run the code that implements the observer pattern and that solves the situation introduced in the prvious
execution, just run the next:
```shell
pipenv run observer_solution
```


# Special thanks to..
For the opportunity to share it with you:
https://codurance.com/

For THE book:

- https://github.com/egamma

- Richard Helm

- https://github.com/smalltalker55

- John Vlissides

